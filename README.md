# Ticket System

Work in Progress (W.I.P.)

Basic Web Ticket System for issues.

It is oriented to the management of the technological infrastructure.

## Backend developed with Django

Technologies used:

* Django - The web framework used.
* Django REST Framework - For the API.
* Djoser - Used to authentication with JWT.

## Frontend developed with Angular

Uses JWT authentication system.

## Future updates (coming soon)

* Admin dashboard.
* Resposible interface.
* Statistic of tickets.

## License
[MIT](https://choosealicense.com/licenses/mit/)
