import "./App.css";
import LoginForm from "./components/loginForm";
import React from "react";

function App() {
  return (
    <React.Fragment>
      <div className="App">
        <LoginForm />
      </div>
    </React.Fragment>
  );
}

export default App;
