import React from "react";

const Input = ({ name, label, type }) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input name={name} type={type} className="form-control" />
    </div>
  );
};

export default Input;
