import React from "react";
import Form from "./common/form";

export class LoginForm extends Form {
  render() {
    return (
      <React.Fragment>
        <form>
          {this.renderInput("nombre", "Nombre")}
          {this.renderInput("apellido", "Apellido")}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Acceder")}
        </form>
      </React.Fragment>
    );
  }
}

export default LoginForm;
