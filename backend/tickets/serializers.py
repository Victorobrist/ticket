from rest_framework import serializers
from .models import Ticket, TicketRecords
from customUser.models import User


class ResponsibleSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'name']

    def get_name(self, obj):
        return obj.first_name+' '+obj.last_name


class TicketRecordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TicketRecords
        fields = '__all__'


class TicketSerializer(serializers.ModelSerializer):
    records = TicketRecordsSerializer(source='ticket_records', many=True, read_only=True)
    responsible = ResponsibleSerializer()

    class Meta:
        model = Ticket
        fields = ['id', 'summary', 'state', 'responsible', 'records', 'date_created', 'date_updated']


