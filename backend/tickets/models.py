from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class Ticket(models.Model):

    class States(models.TextChoices):
        OPEN = 'OP', _('Open')
        REOPEN = 'RO', _('Reopen')
        CLOSE = 'CL', _('Close')

    summary = models.TextField()
    state = models.CharField(max_length=2, choices=States.choices, default=States.OPEN)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True, null=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                   related_name="created_by")
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE,
                                    related_name="updated_by")
    responsible = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE,
                                    related_name="responsible")


class TicketRecords(models.Model):
    description = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, related_name="ticket_records")
