from rest_framework import viewsets, status
from rest_framework.response import Response
from .models import Ticket, TicketRecords
from .serializers import TicketSerializer, TicketRecordsSerializer


class TicketViewSet(viewsets.ModelViewSet):
    serializer_class = TicketSerializer
    # queryset = Ticket.objects.all().order_by('id')

    def get_queryset(self):
        user = self.request.user
        queryset = Ticket.objects.all().order_by('id')
        if user is not None:
            if user.username == 'admin':
                queryset = Ticket.objects.all().order_by('id')
            elif user.is_technician:
                queryset = Ticket.objects.filter(responsible=user).order_by('id')
            else:
                queryset = Ticket.objects.filter(created_by=user).order_by('id')
        return queryset

    # def create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #
    #     if serializer.is_valid():
    #         # Save ticket in database
    #         ticket_obj = serializer.save()
    #         # print(serializer.validated_data)
    #
    #         # for ticket_records in request.validated_data['ticket_records']:
    #         #     ticket_records['ticket'] = ticket_obj.id
    #         #
    #         #
    #         # ticket_records_serializer = TicketRecordsSerializer(data=request.data['ticket_records'], many=True)
    #         # if ticket_records_serializer.is_valid():
    #         #     ticket_records_serializer.save()
    #         # # else:
    #         # #     print("Error on Validation ticket Lines")
    #         # #     print(ticket_line_serializer.errors)
    #         #
    #         headers = self.get_success_headers(serializer.data)
    #         return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TicketRecordsViewSet(viewsets.ModelViewSet):
    serializer_class = TicketRecordsSerializer
    queryset = TicketRecords.objects.all()
