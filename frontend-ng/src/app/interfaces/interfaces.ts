export interface TicketObj {
  id: number;
  state: string;
  summary: string;
  responsible: string;
  date_created: string;
  date_updated: string;
  records: [{ [index: string]: any }];
}
