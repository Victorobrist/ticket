//Modules
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';

//Services
import { HttpService } from './services/http.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { AdminAuthGuard } from './services/admin-auth-guard.service';

//Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { TicketFormComponent } from './components/ticket-form/ticket-form.component';
import { TicketDetailComponent } from './components/ticket-detail/ticket-detail.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TicketListComponent } from './components/ticket-list/ticket-list.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { NoAccessComponent } from './components/no-access/no-access.component';
import { TicketStateComponent } from './components/ticket-state/ticket-state.component';
import { HomeComponent } from './components/home/home.component';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TicketFormComponent,
    TicketDetailComponent,
    NavbarComponent,
    NotFoundComponent,
    TicketListComponent,
    AdminDashboardComponent,
    NoAccessComponent,
    TicketStateComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['localhost:8000'],
        authScheme: 'TS ',
      },
    }),
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent,
        children: [
          { path: 'tickets', component: TicketListComponent },
          { path: 'tickets/create', component: TicketFormComponent },
          { path: 'tickets/:id', component: TicketDetailComponent },
          {
            path: 'admin',
            component: AdminDashboardComponent,
            canActivate: [AuthGuard, AdminAuthGuard],
          },
        ],
      },
      { path: 'no-access', component: NoAccessComponent },
      { path: 'login', component: LoginComponent },

      { path: '**', component: NotFoundComponent }, // for others (example 404)
    ]),
  ],
  providers: [HttpService, AuthService, AuthGuard, AdminAuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
