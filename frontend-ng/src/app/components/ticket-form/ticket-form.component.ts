import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ticket-form',
  templateUrl: './ticket-form.component.html',
  styleUrls: ['./ticket-form.component.css'],
})
export class TicketFormComponent implements OnInit {
  devices: any = [];
  inputText: string = 'Algo';

  constructor(private service: HttpService, private route: Router) {}

  ngOnInit(): void {
    /* this.service.get('tickets').subscribe((response) => {
      console.log(response); */

    this.devices.push(
      { id: 1, name: 'PC', code: '100-01-01' }

      /* { id: 2, name: 'Printer', code: '100-02-01' } */
    );
    /* }); */
  }

  onSubmit(form: {}) {
    //console.log(form);
    this.service.post('tickets/', form).subscribe((response) => {
      //console.log(response);
      this.route.navigate(['tickets']);
    });
  }

  cancel() {
    this.route.navigate(['tickets']);
  }
}
