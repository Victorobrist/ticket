import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http.service';
import { TicketObj } from 'src/app/interfaces/interfaces';
import { TicketState } from 'src/app/utils/customTypes';

/* enum TicketState {
  OP = 'Open',
  RO = 'Reopen',
  CL = 'Close',
} */
//Object.freeze(TicketState);

@Component({
  selector: 'ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css'],
})
export class TicketDetailComponent implements OnInit {
  ticket = {} as TicketObj;
  timeSinceCreated: string = '';

  constructor(
    private service: HttpService,
    private routeActv: ActivatedRoute,
    private route: Router
  ) {}

  ngOnInit(): void {
    //let ticketd_id = this.route.snapshot.paramMap.get('id');

    // to subscribe to multiple observables at the same time
    combineLatest([this.routeActv.paramMap, this.routeActv.queryParamMap])
      .pipe(
        switchMap((combined) => {
          //console.log(combined);
          let ticketd_id = combined[0].get('id')!;

          let page = combined[1].get('page');

          return this.service.get(`tickets/${ticketd_id}`);
        })
      )
      .subscribe((ticked) => {
        this.dataToTicket(ticked);
      });
  }

  dataToTicket(data: any) {
    this.ticket.id = data.id;
    this.ticket.summary = data.summary;
    //this.ticket.state = TicketState[data.state as keyof typeof TicketState];
    this.ticket.state = data.state;
    this.ticket.responsible = data.responsible.name;
    this.ticket.date_created = data.date_created;
    this.ticket.date_updated = data.date_updated;
    this.ticket.records = data.records;

    this.timeSinceCreated = this.calculateDays(
      this.ticket.date_created,
      this.ticket.date_updated
    );
  }

  calculateDays(day1: string, day2: string) {
    let day_1: Date = new Date(day1);
    let day_2: Date = new Date(day2);

    let diff: any = day_2.getTime() - day_1.getTime();

    diff = diff / 1000 / 60 / 60 / 24;

    let days = Math.trunc(diff);
    let hours = Math.trunc((diff - days) * 24);
    let msg = days + ' days ' + hours + ' hours';

    return msg;
  }

  backToTickets() {
    this.route.navigate(['/tickets'], {
      queryParams: {
        page: 1,
        order: 'newest',
      },
    });
  }
}
