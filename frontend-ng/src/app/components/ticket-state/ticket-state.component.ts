import { Component, Input, OnInit } from '@angular/core';
import { TicketState } from 'src/app/utils/customTypes';

@Component({
  selector: 'ticket-state',
  templateUrl: './ticket-state.component.html',
  styleUrls: ['./ticket-state.component.css'],
})
export class TicketStateComponent implements OnInit {
  @Input() state: string = '';
  value: string = '';

  constructor() {}

  ngOnInit(): void {
    this.value = TicketState[this.state as keyof typeof TicketState];
  }
}
