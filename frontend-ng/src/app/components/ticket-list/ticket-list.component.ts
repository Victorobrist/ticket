import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { HttpService } from 'src/app/services/http.service';
import { TicketState } from 'src/app/utils/customTypes';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css'],
})
export class TicketListComponent implements OnInit {
  tickets: any = [];

  constructor(
    private route: Router,
    private service: HttpService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.service.get('tickets').subscribe((response) => {
      //console.log(response);
      //this.tickets = response;
      this.dataToTicket(response);
    });
  }

  create() {
    this.route.navigate(['tickets/create']);
  }

  detail(id: number) {
    this.route.navigate([`tickets/${id}`]);
  }

  dataToTicket(data: any) {
    let ticket: any = {};
    for (let i = 0; i < data.length; i++) {
      //console.log(data[i]);
      ticket = {};
      ticket.id = data[i].id;
      ticket.summary = data[i].summary;
      ticket.state = data[i].state;
      //ticket.state = TicketState[data[i].state as keyof typeof TicketState];

      this.tickets.push(ticket);
    }
  }
}
