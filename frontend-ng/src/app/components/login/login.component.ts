import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  invalidLogin: boolean = false;

  constructor(private route: Router, private authService: AuthService) {}

  ngOnInit(): void {}

  signIn(credentials: {}) {
    //console.log(credentials);
    this.authService.login(credentials).subscribe(
      (response) => {
        //console.log(response);
        if (response) this.route.navigate(['/']);
      },
      (error) => {
        //console.log(error);
        if (error === false) this.invalidLogin = true;
      }
    );
  }
}
