import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(credentials: {}) {
    return this.http.post('http://localhost:8000/api/token', credentials).pipe(
      map((response) => {
        //console.log(response);
        let result: any = response;
        if (result && result.access) {
          localStorage.setItem('token', result.access);
          return true;
        }
        return false;
      }),
      catchError((err) => {
        if (err.status === 401) return throwError(false);
        return throwError(err);
      })
    );
  }

  logout() {
    localStorage.removeItem('token');
  }

  isLoggedIn() {
    let jwtHelper = new JwtHelperService();
    /* console.log(jwtHelper.isTokenExpired());
    return jwtHelper.isTokenExpired(); */

    let token = localStorage.getItem('token');

    if (!token) return false;

    let expirationDate = jwtHelper.getTokenExpirationDate(token);
    let isExpired = jwtHelper.isTokenExpired(token);

    //console.log('Expiration ', expirationDate);
    //console.log('isExpired ', isExpired);

    return !isExpired;
  }

  get currentUser() {
    let token = localStorage.getItem('token');
    if (!token) return null;

    return new JwtHelperService().decodeToken(token);
  }
}
