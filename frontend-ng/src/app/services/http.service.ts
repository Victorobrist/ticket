import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private backend_url = 'http://localhost:8000/api/';

  constructor(private http: HttpClient) {}

  get(endpoint: string) {
    return this.http.get(this.backend_url + endpoint);
  }

  post(endpoint: string, data: {}) {
    return this.http.post(this.backend_url + endpoint, data);
  }

  put(data: any) {
    return this.http.put(this.backend_url + '/' + data.id, data);
  }

  patch(id: number, data: any) {
    return this.http.patch(this.backend_url + '/' + id, JSON.stringify(data));
  }

  delete(endpoint: string, id: number) {
    return this.http.delete(this.backend_url + endpoint + id);
  }
}
