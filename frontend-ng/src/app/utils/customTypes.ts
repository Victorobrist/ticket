export enum TicketState {
  OP = 'Open',
  RO = 'Reopen',
  CL = 'Close',
}
Object.freeze(TicketState);
